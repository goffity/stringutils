package com.goffity.stringutils.encoding;

public class ThaiUtils {

	public static String convertStringToUCS2(String s) {
		String converted = "";
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			int c = (int) ch;
			// System.out.println(c);
			if (c < 127) {
				// System.out.println("ENG");
				converted = converted + "00" + Integer.toHexString(c);
			} else {
				converted = converted + "0" + Integer.toHexString(c);
			}
		}
		// System.out.println("Original : " + s);
		// System.out.println("Converted: " + converted.toUpperCase());

		return converted.toUpperCase();
	}

	public static String unicodeCharacterstoJavaEntities(String str) {
		StringBuilder strBuilder = new StringBuilder("");

		char[] ch = str.toCharArray();
		for (int i = 0; i < ch.length; i++) {
			int cha = (int) ch[i];

			String c = "";

			if (cha < 127) {
				c = "00" + Integer.toHexString((int) ch[i]);
			} else {
				c = "0" + Integer.toHexString((int) ch[i]);
			}

			strBuilder.append("\\u");
			// if (c.length() != 4) {
			// c = "0" + c;
			// }
			strBuilder.append(c.toUpperCase());
			// converted += strBuilder.toString();
		}

		return strBuilder.toString();
	}

	public static String Unicode2ASCII(String unicode) {

		StringBuffer ascii = new StringBuffer(unicode);

		int code;

		for (int i = 0; i < unicode.length(); i++) {

			code = (int) unicode.charAt(i);

			if ((0xE01 <= code) && (code <= 0xE5B))

				ascii.setCharAt(i, (char) (code - 0xD60));

		}

		return ascii.toString();

	}

	public static String ASCII2Unicode(String ascii) {

		StringBuffer unicode = new StringBuffer(ascii);

		int code;

		for (int i = 0; i < ascii.length(); i++) {

			code = (int) ascii.charAt(i);

			if ((0xA1 <= code) && (code <= 0xFB))

				unicode.setCharAt(i, (char) (code + 0xD60));

		}

		return unicode.toString();

	}
}
